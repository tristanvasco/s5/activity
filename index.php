<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>s5: Activity</title>
</head>
<body>

	<?php session_start(); ?>
	<!-- Starts the session -->
	<!-- Login Page -->
	<h3>Log in</h3>
	<div>
		<form method="POST" action="./server.php">
			<input type="hidden" name="action" value="login">
			Email: <input type="text" name="email" required>
			Password: <input type="text" name="password" required>

			<button type="submit">Login</button>
		</form>
	</div>


	<!-- Log Out Page -->
	<h3>Log out</h3>

	<div>
		<?php if(isset($_SESSION['users'])): ?>
			<?php foreach ($_SESSION['users'] as $index => $user): ?>
				<form method="POST" action="./server.php">
					<input type="hidden" name="action" value="clear">
					<input type="hidden" name="id" value="<?php echo $index; ?>">
					<?php echo "Hello ," $user->email; ?>
					<button>Logout</button>
				</form>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>

</body>
</html>
